import tracker from './song-tracker';

export const play = (song) => {
  // Do a bunch of stuff...
  tracker.track(song);
};

export default play;
