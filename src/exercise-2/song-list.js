const songs = [
  { id: 1, title: 'Paris', artist: 'Chainsmokers', genre: 'pop' },
  { id: 2, title: 'Sheena Is a Punk Rocker', artist: 'Ramones', genre: 'punk' },
  { id: 3, title: "So What'Cha Want", artist: 'Beastie Boys', genre: 'rap' },
  { id: 4, title: 'Vicarious', artist: 'Tool', genre: 'rock' },
  { id: 5, title: 'How You Remind Me', artist: 'Nickelback', genre: 'rock' },
  { id: 5, title: 'One Step Closer', artist: 'Linkin Park', genre: 'alternative' },
  { id: 6, title: 'Slow Cheetah', artist: 'Red Hot Chili Peppers', genre: 'alternative' },
  { id: 3, title: 'Tha Shiznit', artist: 'Snoop Dogg', genre: 'rap' }
];

export const getAll = () => songs;
// export const getAll = function() {
//   return songs;
// };

export const findByArtist = artist => songs.filter(song => song.artist === artist);
// export const findByArtist = function(artist) {
//   songs.filter(function(song){
//     return song.artist === artist;
//   });
// };

export const findByGenre = genre => songs.filter(song => song.genre === genre);

export default {
  getAll,
  findByArtist,
  findByGenre
};
