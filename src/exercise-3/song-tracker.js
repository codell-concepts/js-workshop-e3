import chalk from 'chalk';

const played = {};

export default {
  track: ({ id }) => {
    played[id] = played[id] ? played[id] + 1 : 1;
  },

  report() {
    for (const key in played) {
      console.log(chalk.yellow.bold(`Song ${key} has been played ${played[key]} time(s)`));
    }
  }
};
