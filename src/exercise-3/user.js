class User {
  constructor(preferences, songs) {
    this._pref = preferences;
    this._songs = songs;
  }

  async getFavoriteSongs() {
    try {
      const genreSongs = this._pref.genre ? await this._songs.findByGenre(this._pref.genre) : [];
      const artistSongs = this._pref.artist ? await this._songs.findByArtist(this._pref.artist) : [];
      return [...genreSongs, ...artistSongs];
    } catch (err) {
      console.log(err);
      return [];
    }
  }

  // getFavoriteSongs() {
  //   return new Promise((resolve, reject) => {
  //     let favorites = [];
  //     this._songs.findByGenre(this._preferences.genre)
  //       .then((found) => {
  //         favorites = [...favorites, ...found];
  //         return this._songs.findByArtist(this._preferences.artist);
  //       })
  //       .then((found) => {
  //         favorites = [...favorites, ...found];
  //         return favorites;
  //       })
  //       .catch((err) => {
  //         console.log(err);
  //         reject(err);
  //       });
  //   });
  // }
}

export default User;
