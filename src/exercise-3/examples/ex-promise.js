// Promises Introduction
class Parks{

    //Resolve : the function of the successful return result
    //Reject : A function of the failed result

    UserOldEnough(theAge){
    return new Promise(function(resolve,reject){
        
        if(theAge > 6)
        {
            resolve(true);
        }
        else
        {
            reject(`you're is not old enough for the ride :(`);
        }
        });
    }

    UserTallEnough(theHeight){
    return new Promise(function(resolve,reject){
        
        if(theHeight > 48)
        {
            resolve(true);
        }
        else
        {
            reject(`you're not tall enough for the ride :(`);
        }
        });
    }

    //Chained Thens
    goToCalgaryStampedePark(){

        var user = { name:"Johnny Fongtastic", age : 35, height:500}

        this.UserOldEnough(user.age)
        .then(() =>{
            console.log(`Wow, ${user.name} is just old enough.`);
            return this.UserTallEnough(user.height);
        })
        .then(()=>{
            console.log(`${user.name} you must be part tree or something.`);
        })
        .catch((err)=>console.log(err));
    }

    // Async - Await
    async goToEdmontonMallWaterPark()
    {
        var user = { name:"Hairy Henry", age : 35, height:47}

        try
        {
            if(await this.UserOldEnough(user.age) && await this.UserTallEnough(user.height))
            {
                console.log(`Wooo Nessie's Revenge!!`);
            }
        }
        catch(err){
            console.log(`${user.name}, ` + err)
        }
    }
}

export default Parks;